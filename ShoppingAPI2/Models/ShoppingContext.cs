﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace ShoppingAPI2.Models
{
    public class ShoppingContext : DbContext
    {
        public ShoppingContext() : base("ShoppingConnection")  
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Shopping> Shopping { get; set; }
    }
}