﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ShoppingAPI2.Models
{
    public class User
    {
        [Key, Column(Order = 1)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string username { get; set; }
        [Display(Name = "encrypted_password")]
        public string password { get; set; }
        public string email { get; set; }
        public int phone { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public int postcode { get; set; }
        public string name { get; set; }
        public string address { get; set; }

    }
}