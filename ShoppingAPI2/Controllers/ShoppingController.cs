﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ShoppingAPI2.Models;

namespace ShoppingAPI2.Controllers
{
    public class ShoppingController : ApiController
    {
        private ShoppingContext db = new ShoppingContext();

        // GET: api/Shopping
        public IQueryable<Shopping> GetShopping()
        {
            return db.Shopping;
        }

        // GET: api/Shopping/5
        [ResponseType(typeof(Shopping))]
        public IHttpActionResult GetShopping(int id)
        {
            Shopping shopping = db.Shopping.Find(id);
            if (shopping == null)
            {
                return NotFound();
            }

            return Ok(shopping);
        }

        // PUT: api/Shopping/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutShopping(int id, Shopping shopping)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != shopping.id)
            {
                return BadRequest();
            }

            db.Entry(shopping).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShoppingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Shopping
        [ResponseType(typeof(Shopping))]
        public IHttpActionResult PostShopping(Shopping shopping)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Shopping.Add(shopping);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = shopping.id }, shopping);
        }

        // DELETE: api/Shopping/5
        [ResponseType(typeof(Shopping))]
        public IHttpActionResult DeleteShopping(int id)
        {
            Shopping shopping = db.Shopping.Find(id);
            if (shopping == null)
            {
                return NotFound();
            }

            db.Shopping.Remove(shopping);
            db.SaveChanges();

            return Ok(shopping);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShoppingExists(int id)
        {
            return db.Shopping.Count(e => e.id == id) > 0;
        }
    }
}